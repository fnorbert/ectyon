class RawTweet
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  field :brand_id, type: Integer

  field :text, type: String

  field :favorite_count, type: Integer
  field :favorited, type: Boolean

  field :retweet_count, type: Integer
  field :retweeted, type: String

  field :published_at, type: DateTime

  field :tweet_id, type: String
  field :url, type: String

  field :tweet_user_id, type: String
  field :user_screen_name, type: String
  field :user_name, type: String
  field :user_followers_count, type: Integer
  field :user_favorites_count, type: Integer
  field :user_tweets_count, type: Integer
  field :user_profile_image_url, type: String
  field :user_url, type: String
end
