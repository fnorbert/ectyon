class WeeklyTweetContent < ApplicationRecord
  belongs_to :brand
  belongs_to :campaign

  validates_presence_of :brand_id, :campaign_id
  validates_uniqueness_of :week, scope: "campaign_id"
end
