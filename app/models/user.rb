class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :brands
  has_many :campaigns

  mount_uploader :profile_image, ImageUploader

  def full_name
    name = self.first_name.to_s + " " + self.last_name.to_s
    return name.present? ? name.strip : self.email
  end
end
