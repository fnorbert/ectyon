class Brand < ApplicationRecord
  belongs_to :user

  has_many :campaigns

  has_many :tweet_contents
  has_many :daily_tweet_contents
  has_many :weekly_tweet_contents

  has_many :instagram_contents
  has_many :daily_instagram_contents
  has_many :weekly_instagram_contents

  has_one :twitter_stream

  validates_presence_of :name, :user_id

  scope :twitter_credentials, -> {where.not(twitter_auth_token: nil, twitter_auth_secret: nil)}
  scope :instagram_credentials, -> {where.not(instagram_auth_token: nil)}

  def has_twitter_credentials?
    self.twitter_auth_token.present? && self.twitter_auth_secret.present?
  end

  def has_instagram_credentials?
    self.instagram_auth_token.present?
  end
end
