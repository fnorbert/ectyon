class Campaign < ApplicationRecord
  belongs_to :brand
  belongs_to :user

  has_many :tweet_contents
  has_many :daily_tweet_contents
  has_many :weekly_tweet_contents

  has_many :instagram_contents
  has_many :daily_instagram_contents
  has_many :weekly_instagram_contents

  validates_presence_of :name, :user_id, :brand_id

  serialize :or_search_words
  serialize :and_search_words
  serialize :not_search_words

  after_save :pull_tweets, :pull_instagrams
  after_update :update_tweets, :update_instagrams

  def populate_daily_tweets(tweets)
    Resque.enqueue(ResqueLibraries::PopulateDailyTweets, self.brand_id, self.id, tweets)
  end

  def remove_daily_tweets(tweets)
    Resque.enqueue(ResqueLibraries::RemoveDailyTweets, self.brand_id, self.id, tweets)
  end

  def populate_weekly_tweets(tweets)
    Resque.enqueue(ResqueLibraries::PopulateWeeklyTweets, self.brand_id, self.id, tweets)
  end

  def remove_weekly_tweets(tweets)
    Resque.enqueue(ResqueLibraries::RemoveWeeklyTweets, self.brand_id, self.id, tweets)
  end

  def pull_tweets
    Resque.enqueue(ResqueLibraries::PullTweets, self.brand_id, self.id)
  end

  def update_tweets(tweets = nil)
    Resque.enqueue(ResqueLibraries::UpdateTweets, self.brand_id, self.id, tweets)
  end

  def pull_instagrams
    Resque.enqueue(ResqueLibraries::PullInstagrams, self.brand_id, self.id)
  end

  def update_instagrams(instagrams = nil)
    Resque.enqueue(ResqueLibraries::UpdateInstagrams, self.brand_id, self.id, instagrams)
  end

  def populate_daily_instagrams(instagrams)
    Resque.enqueue(ResqueLibraries::PopulateDailyInstagrams, self.brand_id, self.id, instagrams)
  end

  def remove_daily_instagrams(instagrams)
    Resque.enqueue(ResqueLibraries::RemoveDailyInstagrams, self.brand_id, self.id, instagrams)
  end

  def populate_weekly_instagrams(instagrams)
    Resque.enqueue(ResqueLibraries::PopulateWeeklyInstagrams, self.brand_id, self.id, instagrams)
  end

  def remove_weekly_instagrams(instagrams)
    Resque.enqueue(ResqueLibraries::RemoveWeeklyInstagrams, self.brand_id, self.id, instagrams)
  end
end
