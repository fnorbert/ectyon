class TweetContent < ApplicationRecord
  belongs_to :brand
  belongs_to :campaign

  validates_presence_of :brand_id, :campaign_id

  COMPUTE_VELOCITY_EVERY = "1.hours"

  before_save :compute_velocity, :repopulate_daily_tweet, :repopulate_weekly_tweet

  def self.compute_reach(created_at, followers_count, retweet_count, favorite_count)
    # time will be returned is second so it needs to be converted into days
    days = (Time.now - created_at).to_i / (24 * 60 * 60)
    decay = 0
    decay = (days - 2)**3 if days > 3
    # 208 - the avg number of followers
    reach = (followers_count.to_i + retweet_count.to_i * 208 + favorite_count.to_i) - decay
    return reach
  end

  private
    def compute_velocity
      return true if updated_at.nil?
      return true if updated_at > eval(COMPUTE_VELOCITY_EVERY + ".ago")
      return true if reach < 600

      v = (reach - reach_was).to_f / reach * 100

      self.velocity = v.round(2)
    end

    def repopulate_daily_tweet
      if self.favorite_count_changed? || self.retweet_count_changed? ||
          self.reach_changed? || self.velocity_changed?
        day = self.published_at.strftime("%y-%m-%d")
        daily_tweet_content = DailyTweetContent.find_or_create_by(day: day,
                                brand_id: self.brand_id, campaign_id: self.id)
        daily_tweet_content.total_favorite_count += (self.favorite_count - self.favorite_count_was)
        daily_tweet_content.total_retweet_count += (self.retweet_count - self.retweet_count_was)
        daily_tweet_content.total_reach += (self.reach - self.reach_was)
        daily_tweet_content.total_velocity += (self.velocity - self.velocity_was)
        daily_tweet_content.save
      end
    end

    def repopulate_weekly_tweet
      if self.favorite_count_changed? || self.retweet_count_changed? ||
          self.reach_changed? || self.velocity_changed?
        day = self.published_at.beginning_of_week.strftime("%y-%m-%d")
        weekly_tweet_contents = WeeklyTweetContent.find_or_create_by(day: day,
                                brand_id: self.brand_id, campaign_id: self.id)
        weekly_tweet_contents.total_favorite_count += (self.favorite_count - self.favorite_count_was)
        weekly_tweet_contents.total_retweet_count += (self.retweet_count - self.retweet_count_was)
        weekly_tweet_contents.total_reach += (self.reach - self.reach_was)
        weekly_tweet_contents.total_velocity += (self.velocity - self.velocity_was)
        weekly_tweet_contents.save
      end
    end
end
