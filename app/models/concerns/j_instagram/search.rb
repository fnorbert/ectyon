module JInstagram
  class Search < JInstagram::Base
    def populate
      or_keywords = @campaign.or_search_words.split(",")
      and_keywords = @campaign.and_search_words.split(",")
      not_keywords = @campaign.not_search_words.split(",")

      keywords = (or_keywords + and_keywords).uniq

      columns = [:instagram_id, :tags, :text, :location, :comments_count, :likes_count, :published_at,
                 :url, :data_type, :data_link, :user_name, :user_profile_picture,
                 :instagram_user_id, :user_full_name, :brand_id, :campaign_id]
      instagrams = []
      global_instagram_ids = []
      keywords.each do |keyword|
        keyword = keyword.gsub(" ", "")
        @client.tag_recent_media(keyword).each do |instagram|
          if !global_instagram_ids.include?(instagram["id"])
            global_instagram_ids << instagram["id"]
            insta = get_instagram_data([], instagram)
            insta = get_instagram_user_data(insta, instagram)
            insta << @brand.id
            insta << @campaign.id
            instagrams << insta
          end
        end
      end
      new_instagrams, update_instagrams = JInstagram::Validator.new(@campaign, instagrams).validate_new

      if new_instagrams.present?
        import = InstagramContent.import columns, new_instagrams
        populate_instagrams = InstagramContent.where(id: import.ids)
        @campaign.populate_daily_instagrams(populate_instagrams)
        @campaign.populate_weekly_instagrams(populate_instagrams)
      end
      @campaign.update_instagrams(update_instagrams) if update_instagrams.present?
    end

    private
      def get_instagram_data(t, instagram)
        t << instagram.id
        t << instagram.tags.join(",")
        t << instagram.caption.text
        t << instagram.location.name
        t << instagram.comments.count
        t << instagram.likes.count
        t << Time.at(instagram.created_time.to_i)
        t << instagram.link
        t << instagram.type
        if instagram.type == "image"
          t << instagram.images.standard_resolution.url rescue ""
        elsif instagram.type == "video"
          t << instagram.videos.standard_resolution.url rescue ""
        else
          t << ""
        end
        return t
      end

      def get_instagram_user_data(t, instagram)
        user = instagram.user
        t << user.username
        t << user.profile_picture
        t << user.id
        t << user.full_name
        return t
      end
  end
end
