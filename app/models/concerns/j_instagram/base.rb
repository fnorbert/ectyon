module JInstagram
  class Base
    def initialize(brand_id, campaign_id)
      @brand = Brand.find(brand_id)
      @campaign = Campaign.find(campaign_id)

      @client = Instagram.client(access_token: @brand.instagram_auth_token)
    end
  end
end
