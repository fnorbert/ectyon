module JInstagram
  class Validator
    def initialize(campaign, instagrams = nil)
      campaign = campaign
      @or_keywords = campaign.or_search_words.split(",")
      @and_keywords = campaign.and_search_words.split(",")
      @not_keywords = campaign.not_search_words.split(",")
      if !instagrams.nil?
        @instagrams = instagrams
        @db_instagrams_ids = InstagramContent.where(campaign_id: campaign.id,
                    instagram_id: @instagrams.map{|instagram| instagram[0]}).pluck(:instagram_id)
      end
    end

    def validate_new
      new_instagrams = []
      update_instagrams = []
      @instagrams.each do |instagram|
        if is_tag_valid?(instagram[1])
          if is_instagram_valid?(instagram[0])
            new_instagrams << instagram
          else
            update_instagrams << instagram
          end
        end
      end
      return new_instagrams, update_instagrams
    end

    def validate_existing(instagrams)
      valid_instagrams = []
      invalid_instagrams = []
      instagrams.each do |instagram|
        if is_text_valid?(instagram.text)
          valid_instagrams << instagram
        else
          invalid_instagrams << instagram
        end
      end
      return valid_instagrams.flatten, invalid_instagrams.flatten
    end

    private
      def is_tag_valid?(tags)
        tags = tags.split(",")
        has_or_keywords = @or_keywords.map{|word| tags.include?(word)}.include?(true)
        has_and_keywords = !@and_keywords.map{|word| tags.include?(word)}.include?(false)
        has_not_keywords = !@not_keywords.map{|word| !tags.include?(word)}.include?(false)

        return has_or_keywords && has_and_keywords && has_not_keywords
      end

      def is_instagram_valid?(instagram_id)
        return !@db_instagrams_ids.include?(instagram_id)
      end
  end
end
