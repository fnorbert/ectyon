module JInstagram
  class Update < JInstagram::Base
    def given(instagrams)
      db_instagrams = InstagramContent.where(campaign_id: @campaign.id,
                    instagram_id: instagrams.map{|instagram| instagram[0].to_s})
      Lds.ds.store_array(db_instagrams)
      instagrams.each do |instagram|
        db_instagram = Lds.ds.find_by(InstagramContent, {instagram_id: instagram[0].to_s,
                                                  campaign_id: @campaign.id}).first
        db_instagram.tags = instagram[1]
        db_instagram.location = instagram[2]
        db_instagram.comments_count = instagram[3]
        db_instagram.likes_count = instagram[4]
        db_instagram.save
      end
    end

    def existing
      instagrams = InstagramContent.where(campaign_id: @campaign.id)
      Lds.ds.store_array(instagrams)
      valid_instagrams, invalid_instagrams = JInstagram::Validator.new(@campaign).validate_existing(instagrams)
      @campaign.remove_daily_instagrams(invalid_instagrams)
      @campaign.remove_weekly_instagrams(invalid_instagrams)
      InstagramContent.delete_all(["id in (?)", invalid_instagrams.map(&:id)])
    end

  end
end
