module JTwitter
  class Search < JTwitter::Base
    def populate
      or_keywords = @campaign.or_search_words.split(",")
      and_keywords = @campaign.and_search_words.split(",")
      not_keywords = @campaign.not_search_words.split(",")

      keywords = or_keywords.map{|word| "\"#{word}\""}.join(" OR ")
      keywords += and_keywords.map{|word| "\"#{word}\""}.join(" ")
      keywords += not_keywords.map{|word| "-\"#{word}\""}.join(" ")

      sentiment = JTwitter::Sentiment.new(threshold: 0.5)
      sentiment.load_defaults

      columns = [:text, :favorite_count, :favorited, :retweet_count, :retweeted,
                 :published_at, :tweet_id, :url, :reach, :tweet_user_id,
                 :user_screen_name, :user_name, :user_followers_count,
                 :user_favorites_count, :user_tweets_count, :user_profile_image_url,
                 :user_url, :sentiment, :sentiment_score, :brand_id, :campaign_id]
      tweets = []
      @client.search("#{keywords} -rt", lang: "en").take(25).collect do |tweet|
        t = get_tweet_data([], tweet)
        t = compute_reach(t, tweet)
        t = get_tweet_user_data(t, tweet)
        t = get_tweet_sentiment(t, tweet, sentiment)
        t << @brand.id
        t << @campaign.id
        tweets << t
      end
      new_tweets, update_tweets = JTwitter::Validator.new(@campaign, tweets).validate_new
      if new_tweets.present?
        import = TweetContent.import columns, new_tweets
        populate_tweets = TweetContent.where(id: import.ids)
        @campaign.populate_daily_tweets(populate_tweets)
        @campaign.populate_weekly_tweets(populate_tweets)
      end
      @campaign.update_tweets(update_tweets) if update_tweets.present?
    end

    private
      def get_tweet_data(t, tweet)
        t << tweet.text
        t << tweet.favorite_count
        t << tweet.favorited?
        t << tweet.retweet_count
        t << tweet.retweeted?
        t << tweet.created_at
        t << tweet.id
        t << tweet.url.to_s
        return t
      end

      def get_tweet_user_data(t, tweet)
        user = tweet.user
        t << user.id
        t << user.screen_name
        t << user.name
        t << user.followers_count
        t << user.favorites_count
        t << user.tweets_count
        t << user.profile_image_url.to_s
        t << user.url.to_s
        return t
      end

      def get_tweet_sentiment(t, tweet, sentiment)
        t << sentiment.sentiment(tweet.text)
        t << sentiment.score(tweet.text)
        return t
      end

      def compute_reach(t, tweet)
        reach = TweetContent.compute_reach(tweet.created_at,
                  tweet.user.followers_count, tweet.retweet_count, tweet.favorite_count)
        t << reach
        return t
      end

  end
end
