module JTwitter
  class Validator
    def initialize(campaign, tweets = nil)
      campaign = campaign
      @or_keywords = campaign.or_search_words.split(",")
      @and_keywords = campaign.and_search_words.split(",")
      @not_keywords = campaign.not_search_words.split(",")
      if !tweets.nil?
        @tweets = tweets
        @db_tweet_ids = TweetContent.where(campaign_id: campaign.id,
                    tweet_id: @tweets.map{|tweet| tweet[6].to_s}).pluck(:tweet_id)
      end
    end

    def validate_new
      new_tweets = []
      update_tweets = []
      @tweets.each do |tweet|
        if is_text_valid?(tweet[0])
          if is_tweet_valid?(tweet[6])
            new_tweets << tweet
          else
            update_tweets << tweet
          end
        end
      end
      return new_tweets, update_tweets
    end

    def validate_existing(tweets)
      valid_tweets = []
      invalid_tweets = []
      tweets.each do |tweet|
        if is_text_valid?(tweet.text)
          valid_tweets << tweet
        else
          invalid_tweets << tweet
        end
      end
      return valid_tweets.flatten, invalid_tweets.flatten
    end

    private
      def is_text_valid?(text)
        has_or_keywords = @or_keywords.map{|word| text.downcase.include?(word)}.include?(true)
        has_and_keywords = !@and_keywords.map{|word| text.downcase.include?(word)}.include?(false)
        has_not_keywords = !@not_keywords.map{|word| !text.downcase.include?(word)}.include?(false)

        return has_or_keywords && has_and_keywords && has_not_keywords
      end

      def is_tweet_valid?(tweet_id)
        return !@db_tweet_ids.include?(tweet_id.to_s)
      end
  end
end
