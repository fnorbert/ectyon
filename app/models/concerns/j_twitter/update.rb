module JTwitter
  class Update < JTwitter::Base
    def given(tweets)
      db_tweets = TweetContent.where(campaign_id: @campaign.id,
                    tweet_id: tweets.map{|tweet| tweet[6].to_s})
      Lds.ds.store_array(db_tweets)
      tweets.each do |tweet|
        db_tweet = Lds.ds.find_by(TweetContent, {tweet_id: tweet[6].to_s,
                                                  campaign_id: @campaign.id}).first
        db_tweet.favorite_count = tweet[1]
        db_tweet.favorited = tweet[2]
        db_tweet.retweet_count = tweet[3]
        db_tweet.retweeted = tweet[4]
        db_tweet.user_followers_count = tweet[12]
        db_tweet.user_favorites_count = tweet[13]
        db_tweet.user_tweets_count = tweet[14]
        db_tweet.user_profile_image_url = tweet[15]
        db_tweet.reach = TweetContent.compute_reach(tweet[5], tweet[12], tweet[3], tweet[1])
        db_tweet.save
      end
    end

    def existing
      tweets = TweetContent.where(campaign_id: @campaign.id)
      Lds.ds.store_array(tweets)
      valid_tweets, invalid_tweets = JTwitter::Validator.new(@campaign).validate_existing(tweets)
      @campaign.remove_daily_tweets(invalid_tweets)
      @campaign.remove_weekly_tweets(invalid_tweets)
      TweetContent.delete_all(["id in (?)", invalid_tweets.map(&:id)])
    end

  end
end
