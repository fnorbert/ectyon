module JTwitter
  class Stream

    def initialize(brand_id)
      @brand = Brand.find(brand_id)

      @client = Twitter::Streaming::Client.new do |config|
        config.consumer_key        = ENV['TWITTER_KEY']
        config.consumer_secret     = ENV['TWITTER_SECRET']
        config.access_token        = @brand.twitter_auth_token
        config.access_token_secret = @brand.twitter_auth_secret
      end

      @topics = @brand.campaigns.pluck(:or_search_words, :and_search_words).flatten.map{|words| words.split(",")}.flatten
      @ts = @brand.twitter_stream
    end

    def start
      if @topics.present?
        thread = Thread.new do
          begin
            run
            sleep(10)
          rescue Exception => e
            puts "Exception #{e.class.to_s}"
            puts e.message
          end
        end

        @ts.thread_id = thread.object_id
        @ts.status = "running"
        @ts.save
      end
    end

    def stop
      Thread.list.each do |thread|
        if thread.object_id.to_s == @ts.thread_id
          Thread.kill(thread)
          @ts.thread_id = nil
          @ts.status = "stop"
          @ts.save
        end
      end
    end

    private
      def run
        @client.filter(track: @topics.join(",")) do |object|
          if object.is_a?(Twitter::Tweet)
            user = object.user
            RawTweet.create(
              brand_id: @brand.id,
              text: object.text,
              favorite_count: object.favorite_count,
              favorited: object.favorited?,
              retweet_count: object.retweet_count,
              retweeted: object.retweeted?,
              published_at: object.created_at,
              tweet_id: object.id,
              url: object.url.to_s,
              tweet_user_id: user.id,
              user_screen_name: user.screen_name,
              user_name: user.name,
              user_followers_count: user.followers_count,
              user_favorites_count: user.favorites_count,
              user_tweets_count: user.tweets_count,
              user_profile_image_url: user.profile_image_url.to_s,
              user_url: user.url.to_s,
            )
          end
        end
      end
  end
end
