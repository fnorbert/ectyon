class InstagramContent < ApplicationRecord
  belongs_to :brand
  belongs_to :campaign

  validates_presence_of :brand_id, :campaign_id

  before_save :repopulate_daily_instagram, :repopulate_weekly_instagram

  private
    def repopulate_daily_instagram
      if self.comments_count_changed? || self.likes_count_changed?
        day = self.published_at.strftime("%y-%m-%d")
        daily_instagram_content = DailyInstagramContent.find_or_create_by(day: day,
                                brand_id: self.brand_id, campaign_id: self.id)
        daily_instagram_content.total_comments_count += (self.comments_count - self.comments_count_was)
        daily_instagram_content.total_likes_count += (self.total_likes - self.total_likes_was)
        daily_instagram_content.save
      end
    end

    def repopulate_weekly_instagram
      if self.comments_count_changed? || self.likes_count_changed?
        day = self.published_at.beginning_of_week.strftime("%y-%m-%d")
        weekly_instagram_contents = WeeklyInstagramContent.find_or_create_by(day: day,
                                brand_id: self.brand_id, campaign_id: self.id)
        weekly_instagram_contents.total_comments_count += (self.total_comments - self.total_comments_was)
        weekly_instagram_contents.total_likes_count += (self.total_likes - self.total_likes_was)
        weekly_instagram_contents.save
      end
    end
end
