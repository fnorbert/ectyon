ActiveAdmin.register TwitterStream do
  menu parent: "Tweets"

  index do
    selectable_column
    id_column
    column :brand
    column :status
    column :activate do |twitter_stream|
      if twitter_stream.status == "running" || twitter_stream.status == "start"
        link_to "Stop", stop_admin_twitter_stream_path(twitter_stream), method: :post
      else
        link_to "Start", start_admin_twitter_stream_path(twitter_stream), method: :post
      end
    end
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    inputs 'Details' do
      input :thread_id
      input :brand_id
      input :status
    end
    actions
  end

  member_action :start, method: :post do
    ts = TwitterStream.find(params[:id])
    ts.status = "start"
    ts.save
    flash[:notice] = "Twitter stream started"
    redirect_to admin_twitter_streams_path
  end

  member_action :stop, method: :post do
    ts = TwitterStream.find(params[:id])
    ts.status = "stop"
    ts.save
    flash[:notice] = "Twitter stream stopped"
    redirect_to admin_twitter_streams_path
  end
end
