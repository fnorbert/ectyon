ActiveAdmin.register Brand do
  filter :user
  filter :campaign
  filter :name
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    id_column
    column :name
    column :user
    column :has_twitter_credentials do |brand|
      brand.has_twitter_credentials?
    end
    column :created_at
    actions
  end
end
