module InstagramsHelper
  def filter_link_instagram(brand, campaign, order_by)
    if campaign.present?
      order_by_brand_campaign_instagrams_path(brand, campaign, order_by: order_by)
    else
      order_by_brand_instagrams_path(brand, order_by: order_by)
    end
  end

  def filter_class_instagram(order_by)
    params[:order_by] == order_by ? "btn btn-primary btn-xs show-loader" : "btn btn-info btn-xs show-loader"
  end

  def liked_link_instagram(brand, campaign, instagram)
    if campaign.present?
      if instagram.liked
        unlike_brand_campaign_instagram_path(brand, campaign, instagram)
      else
        like_brand_campaign_instagram_path(brand, campaign, instagram)
      end
    else
      if instagram.liked
        unlike_brand_instagram_path(brand, instagram)
      else
        like_brand_instagram_path(brand, instagram)
      end
    end
  end

  def comment_link_instagram(brand, campaign, instagram)
    if campaign.present?
      comments_brand_campaign_instagram_path(brand, campaign, instagram)
    else
      comments_brand_instagram_path(brand, instagram)
    end
  end

  def reply_link_instagram(brand, campaign, instagram)
    if campaign.present?
      reply_brand_campaign_instagram_path(brand, campaign, instagram)
    else
      reply_brand_instagram_path(brand, instagram)
    end
  end

  def follow_link_instagram(brand, campaign, instagram)
    if campaign.present?
      if instagram.followed
        unfollow_brand_campaign_instagram_path(brand, campaign, instagram)
      else
        follow_brand_campaign_instagram_path(brand, campaign, instagram)
      end
    else
      if instagram.followed
        unfollow_brand_instagram_path(brand, instagram)
      else
        follow_brand_instagram_path(brand, instagram)
      end
    end
  end
end
