module TweetsHelper
  def favorite_link(brand, campaign, tweet)
    if campaign.present?
      if tweet.favorited
        unfavorite_brand_campaign_tweet_path(brand, campaign, tweet)
      else
        favorite_brand_campaign_tweet_path(brand, campaign, tweet)
      end
    else
      if tweet.favorited
        unfavorite_brand_tweet_path(brand, tweet)
      else
        favorite_brand_tweet_path(brand, tweet)
      end
    end
  end

  def retweet_link(brand, campaign, tweet)
    if campaign.present?
      if tweet.retweeted
        unretweet_brand_campaign_tweet_path(brand, campaign, tweet)
      else
        retweet_brand_campaign_tweet_path(brand, campaign, tweet)
      end
    else
      if tweet.retweeted
        unretweet_brand_tweet_path(brand, tweet)
      else
        retweet_brand_tweet_path(brand, tweet)
      end
    end
  end

  def follow_link(brand, campaign, tweet)
    if campaign.present?
      if tweet.followed
        unfollow_brand_campaign_tweet_path(brand, campaign, tweet)
      else
        follow_brand_campaign_tweet_path(brand, campaign, tweet)
      end
    else
      if tweet.followed
        unfollow_brand_tweet_path(brand, tweet)
      else
        follow_brand_tweet_path(brand, tweet)
      end
    end
  end

  def post_reply_link(brand, campaign, tweet)
    if campaign.present?
      post_reply_brand_campaign_tweet_path(brand, campaign, tweet)
    else
      post_reply_brand_tweet_path(brand, tweet)
    end
  end

  def reply_link(brand, campaign, tweet)
    if campaign.present?
      reply_brand_campaign_tweet_path(brand, campaign, tweet)
    else
      reply_brand_tweet_path(brand, tweet)
    end
  end

  def comment_link(brand, campaign, tweet)
    if campaign.present?
      comments_brand_campaign_tweet_path(brand, campaign, tweet)
    else
      comments_brand_tweet_path(brand, tweet)
    end
  end

  def filter_link(brand, campaign, order_by)
    if campaign.present?
      order_by_brand_campaign_tweets_path(brand, campaign, order_by: order_by)
    else
      order_by_brand_tweets_path(brand, order_by: order_by)
    end
  end

  def filter_class(order_by)
    params[:order_by] == order_by ? "btn btn-primary btn-xs show-loader" : "btn btn-info btn-xs show-loader"
  end
end
