module DashboardHelper
  def is_dashboard?
     params[:controller] == "brands" && params[:action] == "index"
  end

  def is_brand?
    params[:controller] == "brands" && params[:action] != "index"
  end
end
