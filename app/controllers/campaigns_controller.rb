class CampaignsController < DashboardController

  before_filter :load_brand
  before_filter :new_campaign, only: [:new]
  before_filter :load_campaign, only: [:edit, :update, :pull_tweets,
                                       :pull_instagrams, :destroy]

  def index
    @campaigns = @brand.campaigns
  end

  def create
    @campaign = Campaign.new(campaign_params)
    if @campaign.save
      flash[:success] = "Creation successful"
      redirect_to brand_campaigns_path(@brand)
    else
      flash[:error] = @campaign.errors.full_messages.to_sentence
      render :new
    end
  end

  def update
    if @campaign.update(campaign_params)
      flash[:success] = "Update successful"
      redirect_to brand_campaigns_path(@brand)
    else
      flash[:error] = @campaign.errors.full_messages.to_sentence
      render :edit
    end
  end

  def pull_tweets
    @campaign.pull_tweets
    flash[:success] = "Started pulling tweets"
    redirect_to brand_campaigns_path(@brand)
  end

  def pull_instagrams
    @campaign.pull_instagrams
    flash[:success] = "Started pulling instagrams"
    redirect_to brand_campaigns_path(@brand)
  end

  def destroy
    TweetContent.where(campaign_id: @campaign.id).delete_all
    DailyTweetContent.where(campaign_id: @campaign.id).delete_all
    WeeklyTweetContent.where(campaign_id: @campaign.id).delete_all

    InstagramContent.where(campaign_id: @campaign.id).delete_all
    DailyInstagramContent.where(campaign_id: @campaign.id).delete_all
    WeeklyInstagramContent.where(campaign_id: @campaign.id).delete_all

    @campaign.delete

    flash[:success] = "Destroy successful"
    redirect_to brand_campaigns_path(@brand)
  end

  private
    def campaign_params
      params.require(:campaign).permit(:name, :user_id, :brand_id,
        :or_search_words, :and_search_words, :not_search_words)
    end

    def new_campaign
      @campaign = Campaign.new
    end
end
