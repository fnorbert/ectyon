class BrandsController < DashboardController

  before_filter :new_brand, only: [:new]
  before_filter :load_brand, only: [:edit, :update, :link_social_media]
  before_filter :load_data, only: [:index]

  DATA = %w(general_information tweets_by_week instagrams_by_week
            tweets_reach_velocity_by_week instagrams_likes_comments_by_week)

  def index
    @data = {}
    DATA.flatten.each do |c|
      @data[c.to_sym] = {}
      @data[c.to_sym] = self.send(c)
    end
  end

  def create
    @brand = Brand.new(brand_params)
    if @brand.save
      flash[:success] = "Creation successful"
      redirect_to link_social_media_brand_path(@brand)
    else
      flash[:error] = @brand.errors.full_messages.to_sentence
      render :new
    end
  end

  def update
    if @brand.update(brand_params)
      flash[:success] = "Update successful"
      redirect_to brands_path()
    else
      flash[:error] = @brand.errors.full_messages.to_sentence
      render :edit
    end
  end

  private
    def brand_params
      params.require(:brand).permit(:name, :user_id)
    end

    def new_brand
      @brand = Brand.new
    end

    def load_data
      brands_ids = @brands.pluck(:id)
      @weekly_instagrams = WeeklyInstagramContent.where(brand_id: brands_ids).order('week')
      @instagram_labels = @weekly_instagrams.map {|instagram| instagram.week.to_datetime.strftime("%d %b %Y")}
      @weekly_tweets = WeeklyTweetContent.where(brand_id: brands_ids).order('week')
      @tweet_labels = @weekly_tweets.map {|tweet| tweet.week.to_datetime.strftime("%d %b %Y")}
      @tweets = TweetContent.where(brand_id: brands_ids).limit(10).order("RANDOM()")
      @instagrams = InstagramContent.where(brand_id: brands_ids).limit(4).order("RANDOM()")
    end

    def general_information
      data = {}
      data[:total_instagrams] = @weekly_instagrams.pluck(:total).sum
      data[:instagram_likes] = @weekly_instagrams.pluck(:total_likes_count).sum
      data[:total_tweets] = @weekly_tweets.pluck(:total).sum
      data[:tweets_reach] = @weekly_tweets.pluck(:total_reach).sum.to_i
      return data
    end

    def tweets_by_week
      data = {}
      data[:labels] = @tweet_labels
      data[:datasets] = @weekly_tweets.pluck(:total)
      return data
    end

    def instagrams_by_week
      data = {}
      data[:labels] = @instagram_labels
      data[:datasets] = @weekly_instagrams.pluck(:total)
      return data
    end

    def tweets_reach_velocity_by_week
      data = {}
      data[:labels] = @tweet_labels
      data[:datasets_labels] = ["Reach", "Velocity"]
      data[:datasets] = [@weekly_tweets.pluck(:total_reach), @weekly_tweets.pluck(:total_velocity)]
      return data
    end

    def instagrams_likes_comments_by_week
      data = {}
      data[:labels] = @instagram_labels
      data[:datasets_labels] = ["Likes", "Comments"]
      data[:datasets] = [@weekly_instagrams.pluck(:total_likes_count), @weekly_instagrams.pluck(:total_comments_count)]
      return data
    end
end
