class InstagramStatisticsController < DashboardController

  before_filter :load_brand_data, :load_campaign_data

  def index
    @data = JInstagram::Statistics.new(@brand, @campaign).data
  end

  private
    def load_brand_data
      if params[:brand_id].present? && params[:campaign_id].nil?
        @brand = Brand.find(params[:brand_id])
      end
    end

    def load_campaign_data
      if params[:brand_id].present? && params[:campaign_id].present?
        @brand = Brand.find(params[:brand_id])
        @campaign = Campaign.find(params[:campaign_id])
      end
    end
end
