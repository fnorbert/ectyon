class DashboardController < ApplicationController
  before_action :authenticate_user!
  layout "dashboard"

  before_filter :load_brands

  PER_PAGE = 10

  private
    def load_brands
      @brands = Brand.where(user_id: current_user.id)
    end

    def load_brand
      @brand = Brand.find(params[:brand_id] || params[:id]) 
    end

    def load_campaign
      @campaign = Campaign.find(params[:id])
    end
end
