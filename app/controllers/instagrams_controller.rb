class InstagramsController < DashboardController

  before_filter :load_brand_data, :load_campaign_data
  before_filter :load_instagram, except: [:index, :order_by]
  before_filter :load_instagram_user, only: [:reply]
  before_filter :load_instagram_messages, only: [:comments, :reply]

  layout false, only: [:reply, :comments]

  def like
    JInstagram::Controls.new(@brand.id).like(@instagram)
    @instagram.likes_count += 1
    @instagram.liked = true
    @instagram.save
  end

  def unlike
    JInstagram::Controls.new(@brand.id).unlike(@instagram)
    @instagram.likes_count -= 1
    @instagram.liked = false
    @instagram.save
  end

  def follow
    JInstagram::Controls.new(@brand.id).follow(@instagram)
    @instagram.followed = true
    @instagram.save
  end

  def unfollow
    JInstagram::Controls.new(@brand.id).unfollow(@instagram)
    @instagram.followed = false
    @instagram.save
  end

  def post_reply
    JInstagram::Controls.new(@brand.id).post_reply(@instagram, params[:message])
    flash[:success] = "Successfully posted a comment!"
    redirect_to :back
  end

  private
    def load_brand_data
      if params[:brand_id].present? && params[:campaign_id].nil?
        @brand = Brand.find(params[:brand_id])
        @instagrams = InstagramContent.where(brand_id: @brand.id).order(get_order).page(params[:page]).per(PER_PAGE)
      end
    end

    def load_campaign_data
      if params[:brand_id].present? && params[:campaign_id].present?
        @brand = Brand.find(params[:brand_id])
        @campaign = Campaign.find(params[:campaign_id])
        @instagrams = InstagramContent.where(brand_id: @brand.id, campaign: @campaign.id).order(get_order).page(params[:page]).per(PER_PAGE)
      end
    end

    def load_instagram
      @instagram = InstagramContent.find(params[:id])
    end

    def load_instagram_messages
      @instagram_messages = JInstagram::Conversation.new(@brand).messages(@instagram)
    end

    def load_instagram_user
      @instagram_user = JInstagram::Controls.new(@brand.id).current_instagram_user
    end

    def get_order
      if params[:order_by].present?
        return [params[:order_by] => :desc, "published_at" => :desc]
      else
        return ["published_at" => :desc]
      end
    end
end
