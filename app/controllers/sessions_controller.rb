class SessionsController < ApplicationController

  before_filter :load_brand

  def create
    if auth_hash[:provider] == "twitter"
      @brand.update(twitter_auth_token: auth_hash[:credentials][:token],
        twitter_auth_secret: auth_hash[:credentials][:secret])
    elsif auth_hash[:provider] == "instagram"
      @brand.update(instagram_auth_token: auth_hash[:credentials][:token])
    elsif auth_hash[:provider] == "facebook"
      @brand.update(facebook_auth_token: auth_hash[:credentials][:token])
    end
    redirect_to brands_path
  end

  private
    def auth_hash
      request.env['omniauth.auth']
    end

    def load_brand
      @brand = Brand.find(request.env['omniauth.params']['brand_id'])
    end
end
