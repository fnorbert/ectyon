module JInstagram
  class Statistics
    CHARTS = %w(general_information instagrams_by_day likes_comments_by_day
                likes_by_day comments_by_day total_likes_comments
                top_10_users_by_instagrams)

    attr_accessor :data

    def initialize(brand, campaign = nil)
      @brand = brand
      @campaign = campaign
      load_tweets
      load_labels
      load_data
    end

    private
      def load_tweets
        if @campaign.present?
          @instagrams = DailyInstagramContent.where(brand_id: @brand.id, campaign_id: @campaign.id)
        else
          @instagrams = DailyInstagramContent.where(brand_id: @brand.id)
        end
        @instagrams = @instagrams.sort_by{|instagram| instagram.day.to_datetime}
      end

      def load_labels
        @labels = @instagrams.map {|instagram| instagram.day.to_datetime.strftime("%d %b %Y")}
      end

      def load_data()
        @data = {}
        CHARTS.flatten.each do |c|
          @data[c.to_sym] = {}
          @data[c.to_sym] = self.send(c)
        end
      end

      def general_information
        data = {}
        data[:total_instagrams] = @instagrams.pluck(:total).sum
        data[:total_comments_count] = @instagrams.pluck(:total_comments_count).sum
        data[:total_likes_count] = @instagrams.pluck(:total_likes_count).sum
        return data
      end

      def instagrams_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets] = @instagrams.pluck(:total)
        return data
      end

      def likes_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets] = @instagrams.pluck(:total_likes_count)
        return data
      end

      def comments_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets] = @instagrams.pluck(:total_comments_count)
        return data
      end

      def likes_comments_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets_labels] = ["Like", "Comment"]
        data[:datasets] = [@instagrams.pluck(:total_likes_count), @instagrams.pluck(:total_comments_count)]
        return data
      end

      def total_likes_comments
        data = {}
        data[:labels] = ["Likes", "comments"]
        likes = @instagrams.pluck(:total_likes_count).sum
        comments = @instagrams.pluck(:total_comments_count).sum
        data[:values] = [likes, comments]
        return data
      end

      def top_10_users_by_instagrams
        data = {}
        users = InstagramContent.group(:instagram_user_id).count(:id).sort_by {|key, value| value}.reverse.first(10).to_h
        instagrams = InstagramContent.where(instagram_user_id: users.keys).group_by(&:instagram_user_id).sort_by {|key, value| value.count}.reverse.to_h
        instagrams_by_user = {}
        instagrams.each do |key, value|
          v = value.sort_by(&:published_at).last
          user = {user_name: v.user_name, user_full_name: v.user_full_name,
                  user_profile_picture: v.user_profile_picture}
          instagrams_count = value.count
          data = {instagrams_count: instagrams_count}
          instagrams_by_user[key] = {user: user, data: data}
        end
        data[:data] = instagrams_by_user
        return data
      end
  end
end
