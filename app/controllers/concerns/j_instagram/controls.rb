module JInstagram
  class Controls < JInstagram::Base

    def like(instagram)
      @client.like_media(instagram.instagram_id)
    end

    def unlike(instagram)
      @client.unlike_media(instagram.instagram_id)
    end

    def follow(instagram)
      @client.follow_user(instagram.user_id)
    end

    def unfollow(instagram)
      @client.unfollow_user(instagram.user_id)
    end

    def post_reply(instagram, message)
      @client.create_media_comment(instagram.instagram_id, text)
    end

    def current_instagram_user
      return @client.user
    end

  end
end
