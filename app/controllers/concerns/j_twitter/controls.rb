require 'nokogiri'
require 'open-uri'

module JTwitter
  class Controls < JTwitter::Base

    def favorite(tweet)
      @client.favorite(tweet.tweet_id)
    end

    def unfavorite(tweet)
      @client.unfavorite(tweet.tweet_id)
    end

    def retweet(tweet)
      @client.retweet(tweet.tweet_id)
    end

    def unretweet(tweet)
      current_user_id = @client.user.id
      retweets = @client.retweets(tweet.tweet_id)
      retweets.each do |retweet|
        if current_user_id == retweet.user.id
          @client.status_destroy(retweet.id)
          return
        end
      end
    end

    def follow(tweet)
      twitter_tweet = @client.status(tweet.tweet_id)
      @client.follow(twitter_tweet.user.id)
    end

    def unfollow(tweet)
      twitter_tweet = @client.status(tweet.tweet_id)
      @client.unfollow(twitter_tweet.user.id)
    end

    def post_reply(tweet, message)
      @client.update(message, in_reply_to_status_id: tweet.tweet_id)
    end

    def current_twitter_user
      return @client.user
    end

  end
end
