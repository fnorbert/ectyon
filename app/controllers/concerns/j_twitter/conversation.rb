require 'nokogiri'
require 'open-uri'

module JTwitter
  class Conversation

    def initialize(tweet)
      @tweet = tweet
    end

    def messages
      doc = Nokogiri::HTML(open(@tweet.url))

      tweets = {}
      doc.search(".replies-to .tweet").each_with_index do |reply, i|
        tweets[i] = {}
        tweets[i].merge! parseTweet(reply)
        tweets[i].merge! parseUser(reply)
      end

       return tweets.values
    end

    private

      def parseTweet(reply)
        return {
          tweet_id: reply["data-tweet-id"],
          url: "https://twitter.com" + reply["data-permalink-path"],
          text: reply.search(".tweet-text").text.strip
        }
      end

      def parseUser(reply)
        date = reply.search(".tweet-timestamp").first.attributes["title"].value
        date = date.split("-")
        return {
          user_picture: reply.search(".avatar").first.attributes["src"].value,
          user_name: reply["data-name"],
          user_screen_name: "@" + reply["data-screen-name"],
          date: date[1].strip + " " + date[0].strip
        }
      end

  end
end
