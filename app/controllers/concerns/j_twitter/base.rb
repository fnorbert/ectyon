module JTwitter
  class Base
    def initialize(brand_id, campaign_id = nil)
      @brand = Brand.find(brand_id)
      @campaign = Campaign.find_by_id(campaign_id)

      @client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['TWITTER_KEY']
        config.consumer_secret     = ENV['TWITTER_SECRET']
        config.access_token        = @brand.twitter_auth_token
        config.access_token_secret = @brand.twitter_auth_secret
      end
    end
  end
end
