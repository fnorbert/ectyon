module JTwitter
  class Statistics
    CHARTS = %w(general_information tweets_by_day velocity_by_day reach_by_day
                retweet_favorite_by_day reach_velocity_by_day total_retweet_favorite
                total_sentiment sentiment_by_day top_10_tweets_by_reach
                top_10_users_by_tweets)

    attr_accessor :data

    def initialize(brand, campaign = nil)
      @brand = brand
      @campaign = campaign
      load_tweets
      load_labels
      load_data
    end

    private
      def load_tweets
        if @campaign.present?
          @tweets = DailyTweetContent.where(brand_id: @brand.id, campaign_id: @campaign.id)
        else
          @tweets = DailyTweetContent.where(brand_id: @brand.id)
        end
        @tweets = @tweets.sort_by{|tweet| tweet.day.to_datetime}
      end

      def load_labels
        @labels = @tweets.map {|tweet| tweet.day.to_datetime.strftime("%d %b %Y")}
      end

      def load_data()
        @data = {}
        CHARTS.flatten.each do |c|
          @data[c.to_sym] = {}
          @data[c.to_sym] = self.send(c)
        end
      end

      def general_information
        data = {}
        data[:total_tweets] = @tweets.pluck(:total).sum
        data[:total_reach] = @tweets.pluck(:total_reach).sum
        data[:total_favorit] = @tweets.pluck(:total_favorite_count).sum
        data[:total_retweet] = @tweets.pluck(:total_retweet_count).sum
        return data
      end

      def tweets_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets] = @tweets.pluck(:total)
        return data
      end

      def velocity_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets] = @tweets.pluck(:total_velocity)
        return data
      end

      def reach_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets] = @tweets.pluck(:total_reach)
        return data
      end

      def retweet_favorite_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets_labels] = ["Favorite", "Retweet"]
        data[:datasets] = [@tweets.pluck(:total_favorite_count), @tweets.pluck(:total_retweet_count)]
        return data
      end

      def reach_velocity_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets_labels] = ["Reach", "Velocity"]
        data[:datasets] = [@tweets.pluck(:total_reach), @tweets.pluck(:total_velocity)]
        return data
      end

      def total_retweet_favorite
        data = {}
        data[:labels] = ["Retweet", "Favorite"]
        retweet = @tweets.pluck(:total_retweet_count).sum
        favorite = @tweets.pluck(:total_favorite_count).sum
        data[:values] = [retweet, favorite]
        return data
      end

      def total_sentiment
        data = {}
        data[:labels] = ["Positive", "Neutral", "Negative"]
        positive = @tweets.pluck(:total_positive).sum
        neutral = @tweets.pluck(:total_neutral).sum
        negative = @tweets.pluck(:total_negative).sum
        data[:values] = [positive, neutral, negative]
        return data
      end

      def sentiment_by_day
        data = {}
        data[:labels] = @labels
        data[:datasets_labels] = ["Positive", "Neutral", "Negative"]
        data[:datasets] = [@tweets.pluck(:total_positive), @tweets.pluck(:total_neutral), @tweets.pluck(:total_negative)]
        return data
      end

      def top_10_tweets_by_reach
        data = {}
        data[:tweets] = TweetContent.order(reach: :desc).limit(10)
        return data
      end

      def top_10_users_by_tweets
        data = {}
        users = TweetContent.group(:tweet_user_id).count(:id).sort_by {|key, value| value}.reverse.first(10).to_h
        tweets = TweetContent.where(tweet_user_id: users.keys).group_by(&:tweet_user_id).sort_by {|key, value| value.count}.reverse.to_h
        tweets_by_user = {}
        tweets.each do |key, value|
          v = value.sort_by(&:published_at).last
          user = {user_screen_name: v.user_screen_name, user_name: v.user_name,
                  user_profile_image_url: v.user_profile_image_url}
          reach = value.map(&:reach).sum
          velocity = value.map(&:velocity).sum
          data = {reach: reach, velocity: velocity}
          tweets_by_user[key] = {user: user, data: data}
        end
        data[:data] = tweets_by_user
        return data
      end
  end
end
