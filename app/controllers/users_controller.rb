class UsersController < DashboardController

  before_filter :load_user

  def update
    if @user.update(user_params)
      flash[:success] = "Update successful"
      redirect_to edit_user_path(@user)
    else
      flash[:error] = @user.errors.full_messages.to_sentence
      render :edit
    end
  end

  private
    def user_params
      if params[:user][:password].present?
        params.require(:user).permit(:email, :password, :first_name, :last_name, :profile_image)
      else
        params.require(:user).permit(:email, :first_name, :last_name, :profile_image)
      end
    end

    def load_user
      @user = User.find(params[:id])
    end
end
