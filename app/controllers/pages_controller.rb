class PagesController < ApplicationController

  def contact_us
    flash[:notice] = 'Thanks for getting in touch we will reach out to you soon!'
    redirect_to root_path
  end

end
