class TweetsController < DashboardController

  before_filter :load_brand_data, :load_campaign_data
  before_filter :load_tweet, except: [:index, :order_by]
  before_filter :load_tweet_messages, :load_twitter_user, only: [:reply]
  before_filter :load_tweet_messages, only: [:comments]

  layout false, only: [:reply, :comments]

  def favorite
    JTwitter::Controls.new(@brand.id).favorite(@tweet)
    @tweet.favorite_count += 1
    @tweet.favorited = true
    @tweet.save
  end

  def unfavorite
    JTwitter::Controls.new(@brand.id).unfavorite(@tweet)
    @tweet.favorite_count -= 1
    @tweet.favorited = false
    @tweet.save
  end

  def retweet
    JTwitter::Controls.new(@brand.id).retweet(@tweet)
    @tweet.retweet_count += 1
    @tweet.retweeted = true
    @tweet.save
  end

  def unretweet
    JTwitter::Controls.new(@brand.id).unretweet(@tweet)
    @tweet.retweet_count -= 1
    @tweet.retweeted = false
    @tweet.save
  end

  def follow
    JTwitter::Controls.new(@brand.id).follow(@tweet)
    @tweet.followed = true
    @tweet.save
  end

  def unfollow
    JTwitter::Controls.new(@brand.id).unfollow(@tweet)
    @tweet.followed = false
    @tweet.save
  end

  def post_reply
    JTwitter::Controls.new(@brand.id).post_reply(@tweet, params[:message])
    flash[:success] = "Successfully replied to the tweet!"
    redirect_to :back
  end

  private
    def load_brand_data
      if params[:brand_id].present? && params[:campaign_id].nil?
        @brand = Brand.find(params[:brand_id])
        @tweets = TweetContent.where(brand_id: @brand.id).order(get_order).page(params[:page]).per(PER_PAGE)
      end
    end

    def load_campaign_data
      if params[:brand_id].present? && params[:campaign_id].present?
        @brand = Brand.find(params[:brand_id])
        @campaign = Campaign.find(params[:campaign_id])
        @tweets = TweetContent.where(brand_id: @brand.id, campaign: @campaign.id).order(get_order).page(params[:page]).per(PER_PAGE)
      end
    end

    def load_tweet
      @tweet = TweetContent.find(params[:id])
    end

    def load_tweet_messages
      @tweet_messages = JTwitter::Conversation.new(@tweet).messages()
    end

    def load_twitter_user
      @twitter_user = JTwitter::Controls.new(@brand.id).current_twitter_user
    end

    def get_order
      if params[:order_by].present?
        return [params[:order_by] => :desc, "published_at" => :desc]
      else
        return ["published_at" => :desc]
      end
    end
end
