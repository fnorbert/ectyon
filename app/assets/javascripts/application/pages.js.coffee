window.ectyon = window.ectyon or {}
((pages, $) ->

  pages.initSmoothScroll = ->
    $(document).off "click", "a.smooth-scroll"
    $(document).on "click", "a.smooth-scroll", (e) ->
      e.preventDefault()
      id = $(@).attr("href")
      $("html, body").animate
        scrollTop: $(id).offset().top - 100
      , 1500

) window.ectyon.pages = window.ectyon.pages or {}, jQuery
