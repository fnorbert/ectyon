window.ectyon = window.ectyon or {}
((campaigns, $) ->

  campaigns.init_select_tags = ->
    $('.search-tag').select2(
      placeholder: 'Search Term',
      tags: []
    )

) window.ectyon.campaigns = window.ectyon.campaigns or {}, jQuery
