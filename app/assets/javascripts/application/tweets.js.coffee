window.ectyon = window.ectyon or {}
((tweets, $) ->

  endless_scroll = ->
    $(".pagination").hide()
    $(".page-loader").hide()
    url = $(".pagination .next a").attr("href")
    if url && $(window).scrollTop() > $(document).height() - $(window).height() - 150
      $(".page-loader").show()
      $.getScript url
      $(window).off "scroll", endless_scroll

  tweets.init_endless_scroll = ->
    $(window).off "scroll"
    $(window).on "scroll", endless_scroll

) window.ectyon.tweets = window.ectyon.tweets or {}, jQuery
