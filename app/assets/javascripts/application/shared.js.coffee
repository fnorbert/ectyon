window.ectyon = window.ectyon or {}
((shared, $) ->

  shared.init = ->
    init_navbar()
    loader()

  init_navbar = ->
    $(".navbar-expand-toggle").off "click"
    $(".navbar-expand-toggle").on "click", ->
      $(".app-container").toggleClass "expanded"
      $(".navbar-expand-toggle").toggleClass "fa-rotate-90"

    $(".navbar-right-expand-toggle").off "click"
    $(".navbar-right-expand-toggle").on "click", ->
      $(".navbar-right").toggleClass "expanded"
      $(".navbar-right-expand-toggle").toggleClass "fa-rotate-90"

  loader = ->
    $(".show-loader").off "click"
    $(".show-loader").on "click", ->
      $("#loader").addClass("display-block")

  shared.init_loader = ->
    loader()

  shared.hide_loader = ->
    $("#loader").removeClass("display-block")

  shared.init_flash = (message) ->
    if message.length == 0
      $("#flash").hide()
    else
      $("#flash").fadeIn("slow")
      setTimeout (->
        $("#flash").fadeOut("slow")
        return
      ), 4000

) window.ectyon.shared = window.ectyon.shared or {}, jQuery
