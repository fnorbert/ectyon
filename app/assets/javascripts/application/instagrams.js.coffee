window.ectyon = window.ectyon or {}
((instagrams, $) ->

  endless_scroll = ->
    $(".pagination").hide()
    $(".page-loader").hide()
    url = $(".pagination .next a").attr("href")
    if url && $(window).scrollTop() > $(document).height() - $(window).height() - 150
      $(".page-loader").show()
      $.getScript url
      $(window).off "scroll", endless_scroll

  instagrams.init_endless_scroll = ->
    $(window).off "scroll"
    $(window).on "scroll", endless_scroll

) window.ectyon.instagrams = window.ectyon.instagrams or {}, jQuery
