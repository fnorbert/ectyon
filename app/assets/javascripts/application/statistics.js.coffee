window.ectyon = window.ectyon or {}
((statistics, $) ->

  statistics.init_bar_chart = (element_id, animation) ->
    element = $("##{element_id}")
    labels = element.data("labels")
    datasets = element.data("datasets")
    datasets_labels = element.data("datasets-labels")

    options =
      animation: animation
      scaleFontSize: 10
      scaleFontColor: "#363c46"
      responsive: true
      tooltipFillColor: "rgba(54,60,70,0.9)"
      tooltipFontSize: 12
      tooltipTitleFontSize: 13
      scaleShowVerticalLines: false
      multiTooltipTemplate: "<%= value %> <%= datasetLabel %>"
      scaleShowHorizontalLines: false
      scaleShowLabels: false

    data =
      labels: labels
      datasets: [
        {
          label: datasets_labels[0]
          fillColor: "rgba(26,188,156,0.4)",
          strokeColor: "#1ABC9C",
          pointColor: "#1ABC9C",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#1ABC9C",
          data: datasets[0]
        }
        {
          label: datasets_labels[1]
          fillColor: "rgba(34,167,240,0.4)",
          strokeColor: "#22A7F0",
          pointColor: "#22A7F0",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#22A7F0",
          data: datasets[1]
        }
      ]

    new Chart(element.get(0).getContext("2d")).Bar(data, options)

  statistics.init_bar3_chart = (element_id, animation) ->
    element = $("##{element_id}")
    labels = element.data("labels")
    datasets = element.data("datasets")
    datasets_labels = element.data("datasets-labels")

    options =
      animation: animation
      scaleFontSize: 10
      scaleFontColor: "#363c46"
      responsive: true
      tooltipFillColor: "rgba(54,60,70,0.9)"
      tooltipFontSize: 12
      tooltipTitleFontSize: 13
      scaleShowVerticalLines: false
      multiTooltipTemplate: "<%= value %> <%= datasetLabel %>"
      scaleShowHorizontalLines: false
      scaleShowLabels: false

    data =
      labels: labels
      datasets: [
        {
          label: datasets_labels[0]
          fillColor: "rgba(26,188,156,0.4)",
          strokeColor: "#1ABC9C",
          pointColor: "#1ABC9C",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#1ABC9C",
          data: datasets[0]
        }
        {
          label: datasets_labels[1]
          fillColor: "rgba(34,167,240,0.4)",
          strokeColor: "#22A7F0",
          pointColor: "#22A7F0",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#22A7F0",
          data: datasets[1]
        }
        {
          label: datasets_labels[2]
          fillColor: "rgba(250,42,0,0.4)",
          strokeColor: "#fa2a00",
          pointColor: "#fa2a00",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#22A7F0",
          data: datasets[2]
        }
      ]

    new Chart(element.get(0).getContext("2d")).Bar(data, options)

  statistics.init_line_chart = (element_id, animation) ->
    element = $("##{element_id}")
    labels = element.data("labels")
    datasets = element.data("datasets")

    options =
      animation: animation
      scaleFontSize: 10
      scaleFontColor: "#363c46"
      responsive: true
      tooltipFillColor: "rgba(54,60,70,0.9)"
      tooltipFontSize: 12
      tooltipTitleFontSize: 13
      scaleShowVerticalLines: false
      scaleShowHorizontalLines: false
      scaleShowLabels: false

    data =
      labels: labels
      datasets: [
        {
          fillColor: "rgba(34,167,240,0.4)",
          strokeColor: "#22A7F0",
          pointColor: "#22A7F0",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#22A7F0",
          data: datasets
        }
      ]

    new Chart(element.get(0).getContext("2d")).Line(data, options)

  statistics.init_pie_chart = (element_id, animation) ->
    element = $("##{element_id}")
    labels = element.data("labels")
    values = element.data("values")

    options =
      animation: animation
      scaleFontSize: 10
      scaleFontColor: "#363c46"
      responsive: true
      tooltipFillColor: "rgba(54,60,70,0.9)"
      tooltipFontSize: 12
      tooltipTitleFontSize: 13
      scaleShowVerticalLines: false
      tooltipTemplate: "<%= value %> <%= label %>"

    data = [
      {
        value: values[0],
        color:"rgba(26,188,156,1)",
        highlight: "rgba(26,188,156,0.4)",
        label: labels[0]
      }
      {
        value: values[1],
        color: "rgba(34,167,240,1)",
        highlight: "rgba(34,167,240,0.4)",
        label: labels[1]
      }
    ]

    new Chart(element.get(0).getContext("2d")).Pie(data, options)

  statistics.init_pie3_chart = (element_id, animation) ->
    element = $("##{element_id}")
    labels = element.data("labels")
    values = element.data("values")

    options =
      animation: animation
      scaleFontSize: 10
      scaleFontColor: "#363c46"
      responsive: true
      tooltipFillColor: "rgba(54,60,70,0.9)"
      tooltipFontSize: 12
      tooltipTitleFontSize: 13
      scaleShowVerticalLines: false
      tooltipTemplate: "<%= value %> <%= label %>"

    data = [
      {
        value: values[0],
        color:"rgba(26,188,156,1)",
        highlight: "rgba(26,188,156,0.4)",
        label: labels[0]
      }
      {
        value: values[1],
        color: "rgba(34,167,240,1)",
        highlight: "rgba(34,167,240,0.4)",
        label: labels[1]
      }
      {
        value: values[2],
        color: "rgba(250,42,0,1)",
        highlight: "rgba(250,42,0,0.4)",
        label: labels[2]
      }
    ]

    new Chart(element.get(0).getContext("2d")).Pie(data, options)

) window.ectyon.statistics = window.ectyon.statistics or {}, jQuery
