# bundle exec rails runner tproc.rb
sentiment = JTwitter::Sentiment.new(threshold: 0.5)
sentiment.load_defaults
loop do
  Brand.twitter_credentials.each do |brand|
    raw_tweets = []
    brand.campaigns.each do |campaign|
      columns = [:text, :favorite_count, :favorited, :retweet_count, :retweeted,
                 :published_at, :tweet_id, :url, :reach, :tweet_user_id,
                 :user_screen_name, :user_name, :user_followers_count,
                 :user_favorites_count, :user_tweets_count, :user_profile_image_url,
                 :user_url, :sentiment, :sentiment_score, :brand_id, :campaign_id]
      tweets = []
      RawTweet.where(brand_id: brand.id).limit(25).each do |tweet|
        raw_tweets << tweet
        t = []
        t << tweet.text
        t << tweet.favorite_count
        t << tweet.favorited?
        t << tweet.retweet_count
        t << tweet.retweeted?
        t << tweet.published_at
        t << tweet.tweet_id
        t << tweet.url
        reach = TweetContent.compute_reach(tweet.published_at,
                  tweet.user_url, tweet.retweet_count, tweet.favorite_count)
        t << reach
        t << tweet.tweet_user_id
        t << tweet.user_screen_name
        t << tweet.user_name
        t << tweet.user_followers_count
        t << tweet.user_favorites_count
        t << tweet.user_tweets_count
        t << tweet.user_profile_image_url
        t << tweet.user_url
        t << sentiment.sentiment(tweet.text)
        t << sentiment.score(tweet.text)
        t << brand.id
        t << campaign.id
        tweets << t
      end
      new_tweets, update_tweets = JTwitter::Validator.new(campaign, tweets).validate_new
      if new_tweets.present?
        import = TweetContent.import columns, new_tweets
        populate_tweets = TweetContent.where(id: import.ids)
        campaign.populate_daily_tweets(populate_tweets)
        campaign.populate_weekly_tweets(populate_tweets)
      end
      campaign.update_tweets(update_tweets) if update_tweets.present?
    end
    raw_tweets.map(&:delete)
  end
end
