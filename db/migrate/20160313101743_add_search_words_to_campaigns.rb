class AddSearchWordsToCampaigns < ActiveRecord::Migration[5.0]
  def change
    add_column :campaigns, :or_search_words, :string
    add_column :campaigns, :and_search_words, :string
    add_column :campaigns, :not_search_words, :string
  end
end
