class AddFacebookCredentialsToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :facebook_auth_token, :string
  end
end
