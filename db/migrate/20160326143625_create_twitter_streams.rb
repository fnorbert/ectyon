class CreateTwitterStreams < ActiveRecord::Migration[5.0]
  def change
    create_table :twitter_streams do |t|
      t.string :thread_id
      t.integer :brand_id

      t.timestamps
    end
  end
end
