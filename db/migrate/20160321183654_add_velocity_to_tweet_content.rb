class AddVelocityToTweetContent < ActiveRecord::Migration[5.0]
  def change
    add_column :tweet_contents, :velocity, :real, default: 0
  end
end
