class AddFollowedToTweetContents < ActiveRecord::Migration[5.0]
  def change
    add_column :tweet_contents, :followed, :boolean, default: false
  end
end
