class AddInformationToTweetContents < ActiveRecord::Migration[5.0]
  def change
    add_column :tweet_contents, :text, :string
    add_column :tweet_contents, :favorite_count, :integer
    add_column :tweet_contents, :favorited, :boolean
    add_column :tweet_contents, :retweet_count, :integer
    add_column :tweet_contents, :retweeted, :boolean
    add_column :tweet_contents, :published_at, :datetime
    add_column :tweet_contents, :tweet_id, :string
    add_column :tweet_contents, :url, :string
    add_column :tweet_contents, :tweet_user_id, :string
    add_column :tweet_contents, :user_screen_name, :string
    add_column :tweet_contents, :user_name, :string
    add_column :tweet_contents, :user_followers_count, :integer
    add_column :tweet_contents, :user_favorites_count, :integer
    add_column :tweet_contents, :user_tweets_count, :integer
    add_column :tweet_contents, :user_profile_image_url, :string
    add_column :tweet_contents, :user_url, :string
  end
end
