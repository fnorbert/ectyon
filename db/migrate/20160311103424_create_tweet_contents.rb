class CreateTweetContents < ActiveRecord::Migration[5.0]
  def change
    create_table :tweet_contents do |t|
      t.integer :brand_id
      t.integer :campaign_id

      t.timestamps
    end
  end
end
