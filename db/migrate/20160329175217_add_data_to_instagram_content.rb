class AddDataToInstagramContent < ActiveRecord::Migration[5.0]
  def change
    add_column :instagram_contents, :tags, :text
    add_column :instagram_contents, :location, :string
    add_column :instagram_contents, :comments_count, :integer
    add_column :instagram_contents, :followed, :boolean, default: false
    add_column :instagram_contents, :likes_count, :integer
    add_column :instagram_contents, :liked, :boolean, default: false
    add_column :instagram_contents, :published_at, :datetime
    add_column :instagram_contents, :url, :string
    add_column :instagram_contents, :data_type, :string
    add_column :instagram_contents, :data_link, :string
    add_column :instagram_contents, :instagram_id, :string
    add_column :instagram_contents, :text, :string

    add_column :instagram_contents, :user_name, :string
    add_column :instagram_contents, :user_profile_picture, :string
    add_column :instagram_contents, :instagram_user_id, :string
    add_column :instagram_contents, :user_full_name, :string
  end
end
