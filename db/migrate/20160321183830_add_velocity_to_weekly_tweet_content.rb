class AddVelocityToWeeklyTweetContent < ActiveRecord::Migration[5.0]
  def change
    add_column :weekly_tweet_contents, :total_velocity, :real, default: 0
  end
end
