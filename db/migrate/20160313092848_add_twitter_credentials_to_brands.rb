class AddTwitterCredentialsToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :twitter_auth_token, :string
    add_column :brands, :twitter_auth_secret, :string
  end
end
