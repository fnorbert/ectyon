class CreateWeeklyTweetContents < ActiveRecord::Migration[5.0]
  def change
    create_table :weekly_tweet_contents do |t|
      t.integer :brand_id
      t.integer :campaign_id
      t.string :week
      t.integer :total, default: 0
      t.integer :total_favorite_count, default: 0
      t.integer :total_retweet_count, default: 0

      t.timestamps
    end
  end
end
