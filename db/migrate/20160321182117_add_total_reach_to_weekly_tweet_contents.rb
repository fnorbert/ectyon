class AddTotalReachToWeeklyTweetContents < ActiveRecord::Migration[5.0]
  def change
    add_column :weekly_tweet_contents, :total_reach, :real, default: true
  end
end
