class AddReachToTweetContent < ActiveRecord::Migration[5.0]
  def change
    add_column :tweet_contents, :reach, :float
  end
end
