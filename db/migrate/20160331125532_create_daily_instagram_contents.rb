class CreateDailyInstagramContents < ActiveRecord::Migration[5.0]
  def change
    create_table :daily_instagram_contents do |t|
      t.integer :brand_id
      t.integer :campaign_id
      t.string :day
      t.integer :total, default: 0
      t.integer :total_comments_count, default: 0
      t.integer :total_likes_count, default: 0

      t.timestamps
    end
  end
end
