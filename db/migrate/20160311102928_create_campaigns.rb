class CreateCampaigns < ActiveRecord::Migration[5.0]
  def change
    create_table :campaigns do |t|
      t.string :name
      t.integer :user_id
      t.integer :brand_id
      
      t.timestamps
    end
  end
end
