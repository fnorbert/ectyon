class AddInstagramCredentialsToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :instagram_auth_token, :string
  end
end
