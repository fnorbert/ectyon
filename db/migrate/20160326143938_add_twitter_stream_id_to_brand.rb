class AddTwitterStreamIdToBrand < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :twitter_stream_id, :integer
  end
end
