class AddStatusToTwitterStream < ActiveRecord::Migration[5.0]
  def change
    add_column :twitter_streams, :status, :string, default: "stop"
  end
end
