class CreateWeeklyInstagramContents < ActiveRecord::Migration[5.0]
  def change
    create_table :weekly_instagram_contents do |t|
      t.integer :brand_id
      t.integer :campaign_id
      t.string :week
      t.integer :total
      t.integer :total, default: 0
      t.integer :total_comments_count, default: 0
      t.integer :total_likes_count, default: 0
      
      t.timestamps
    end
  end
end
