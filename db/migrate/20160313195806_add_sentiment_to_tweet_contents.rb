class AddSentimentToTweetContents < ActiveRecord::Migration[5.0]
  def change
    add_column :tweet_contents, :sentiment, :string
    add_column :tweet_contents, :sentiment_score, :real
  end
end
