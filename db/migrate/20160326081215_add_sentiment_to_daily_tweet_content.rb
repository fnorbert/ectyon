class AddSentimentToDailyTweetContent < ActiveRecord::Migration[5.0]
  def change
    add_column :daily_tweet_contents, :total_positive, :integer, default: 0
    add_column :daily_tweet_contents, :total_neutral, :integer, default: 0
    add_column :daily_tweet_contents, :total_negative, :integer, default: 0
  end
end
