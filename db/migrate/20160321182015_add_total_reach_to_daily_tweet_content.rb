class AddTotalReachToDailyTweetContent < ActiveRecord::Migration[5.0]
  def change
    add_column :daily_tweet_contents, :total_reach, :real, default: 0
  end
end
