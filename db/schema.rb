# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160406134559) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "twitter_auth_token"
    t.string   "twitter_auth_secret"
    t.string   "instagram_auth_token"
    t.string   "facebook_auth_token"
    t.integer  "twitter_stream_id"
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "brand_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "or_search_words"
    t.string   "and_search_words"
    t.string   "not_search_words"
  end

  create_table "daily_instagram_contents", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.string   "day"
    t.integer  "total",                default: 0
    t.integer  "total_comments_count", default: 0
    t.integer  "total_likes_count",    default: 0
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "daily_tweet_contents", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.string   "day"
    t.integer  "total",                default: 0
    t.integer  "total_favorite_count", default: 0
    t.integer  "total_retweet_count",  default: 0
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.float    "total_reach",          default: 0.0
    t.float    "total_velocity",       default: 0.0
    t.integer  "total_positive",       default: 0
    t.integer  "total_neutral",        default: 0
    t.integer  "total_negative",       default: 0
  end

  create_table "instagram_contents", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.text     "tags"
    t.string   "location"
    t.integer  "comments_count"
    t.boolean  "followed",             default: false
    t.integer  "likes_count"
    t.boolean  "liked",                default: false
    t.datetime "published_at"
    t.string   "url"
    t.string   "data_type"
    t.string   "data_link"
    t.string   "instagram_id"
    t.string   "text"
    t.string   "user_name"
    t.string   "user_profile_picture"
    t.string   "instagram_user_id"
    t.string   "user_full_name"
  end

  create_table "tweet_contents", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "text"
    t.integer  "favorite_count"
    t.boolean  "favorited"
    t.integer  "retweet_count"
    t.boolean  "retweeted"
    t.datetime "published_at"
    t.string   "tweet_id"
    t.string   "url"
    t.string   "tweet_user_id"
    t.string   "user_screen_name"
    t.string   "user_name"
    t.integer  "user_followers_count"
    t.integer  "user_favorites_count"
    t.integer  "user_tweets_count"
    t.string   "user_profile_image_url"
    t.string   "user_url"
    t.string   "sentiment"
    t.float    "sentiment_score"
    t.boolean  "followed",               default: false
    t.float    "reach"
    t.float    "velocity",               default: 0.0
  end

  create_table "twitter_streams", force: :cascade do |t|
    t.string   "thread_id"
    t.integer  "brand_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "status",     default: "stop"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "profile_image"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "weekly_instagram_contents", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.string   "week"
    t.integer  "total",                default: 0
    t.integer  "total_comments_count", default: 0
    t.integer  "total_likes_count",    default: 0
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "weekly_tweet_contents", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "campaign_id"
    t.string   "week"
    t.integer  "total",                default: 0
    t.integer  "total_favorite_count", default: 0
    t.integer  "total_retweet_count",  default: 0
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.float    "total_reach",          default: 1.0
    t.float    "total_velocity",       default: 0.0
    t.integer  "total_positive",       default: 0
    t.integer  "total_neutral",        default: 0
    t.integer  "total_negative",       default: 0
  end

end
