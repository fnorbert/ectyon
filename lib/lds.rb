# Local Data Store
class Lds
  # Return the LDS object of the current thread
  def self.ds
    Thread.current[:ds] ||= Lds.new
  end

  # Invalidate the current LDS
  def invalidate!
    Thread.current[:ds] = Lds.new
  end

  # Find an object, either in local store or from SQL
  #
  # Lds.ds.find(Tee, 7286)
  def find(class_name, id)
    return nil if id.blank?
    variable = get_variable_for_class(class_name)
    store = get_local_store(variable)

    store[id] = class_name.find_by_id(id) if store[id].blank?
    return store[id]
  end

  # Lds.ds.find_index(InvoiceEntry, "member_id", 4751)
  # search value 4751 for index member_id
  def find_index(class_name, index, id)
    return [] if index.blank? || id.blank?
    variable = get_variable_index_for_class(class_name, index)
    store = get_local_store(variable)

    results = []
    id = [id] if !id.is_a?(Array)
    id.each do |val_id|
      field = val_id
      field = field.to_s if id.class.to_s != "String"

      if store.present? && store[field].present?
        results += store[field]
      end
    end
    results
  end

  # Find array of objects, in local store (only in local store)
  # That match a set of conditions, defined by a hash.
  #
  # Lds.ds.find_by(Tee, {course_id: 100})
  def find_by(class_name, conditions)
    variable = get_variable_for_class(class_name)
    store = get_local_store(variable)
    results = []
    store.each do |id, object|
      valid = true
      conditions.each do |field, value|
        valid = false if object.send(field) != value
      end

      results << object if valid
    end
    return results
  end

  # Find array of objects, in local store (only in local store)
  # That match a set of conditions. However, if the condition is
  # of form: "field: [array]", we add an object to the resulting array
  # if any of the array values matches.
  #
  # E.g., Lds.ds.find_array_by(Member, {id: [1, 2, 3]})
  def find_array_by(class_name, conditions)
    variable = get_variable_for_class(class_name)
    store = get_local_store(variable)

    results = []
    store.each do |id, object|
      next if object.blank?

      valid = true
      conditions.each do |field, value|
        if value.class.to_s == "Array"
          valid = false if !value.include?(object.send(field))
        else
          valid = false if object.send(field) != value
        end
      end

      results << object if valid
    end
    return results
  end

  # Store an array of objects, not necessarly the same class,
  # in the Lds
  #
  # Lds.ds.store_array(League.find(1140).courses)
  def store_array(array_of_objects, class_name = nil)
    array_of_objects.each do |obj|
      store_object(obj, class_name) if obj.present?
    end
  end

  def store_variable(variable_name, id, objects)
    variable = "@#{variable_name}"
    store = get_local_store(variable)
    store[id] = objects
  end

  def get_variable(variable_name, id)
    variable = "@#{variable_name}"
    store = get_local_store(variable)
    return [] if id.blank?
    return store[id]
  end

  def get_array(class_name)
    variable = get_variable_for_class(class_name)
    store = get_local_store(variable)
    results = []
    store.each do |key, obj|
      results << obj
    end
    return results
  end

  # Lds.ds.add_index(InvoiceEntry, ["member_id","status"])
  def add_index(class_name, array_of_index=nil)
    return nil if !present_local_store_for_class(class_name)
    return nil if array_of_index.blank?
    if array_of_index.class.to_s != "Array"
      array_of_index = [array_of_index]
    end
    array_of_index.each do |index|
      variable = get_variable_index_for_class(class_name, index)
      store = get_local_store(variable)
      store_index(class_name, store, index)
    end
  end

  # class_name       => InvoiceEntry
  # store            => @invoiceentries_index_by_member_id
  # index            => "member_id"
  def store_index(class_name, store, index)
    array_of_objects = get_array(class_name)
    array_sorted = array_of_objects.sort_by{|x| ["x.#{index}", x.created_at || Time.now]}.group_by{|x| x[index]}
    array_sorted.keys.each do |key|
      field = key
      field = field.to_s if key.class.to_s != "String"
      if store[field].blank?
        store[field] = []
        store[field] = array_sorted[key]
      end
    end
  end

  # Store a single object in the LDS
  #
  # Lds.ds.store_object(Tee.find(7286))
  def store_object(obj, class_name = nil)
    name = class_name.present? ? class_name : obj.class
    variable = get_variable_for_class(name)
    store = get_local_store(variable)
    store[obj.id] = obj
  end

  def get_variable_index_for_class(cls, index)
    variable = "@#{cls.to_s.downcase.gsub(":", "_")}_index_by_#{index.to_s.downcase.gsub(":","")}"
  end

  # Return the name of the local store of this class
  def get_variable_for_class(cls)
    variable = "@#{cls.to_s.downcase.gsub(":", "_")}_by_id"
  end

  # Return or initialize a new local store with this name
  def get_local_store(name)
    local_store = instance_variable_get(name)
    if local_store.blank?
      instance_variable_set(name, {})
      local_store = instance_variable_get(name)
    end
    return local_store
  end

  def present_local_store_for_class(cls)
    variable = get_local_store(get_variable_for_class(cls))
    return variable.present?
  end

  def present_local_store_for_variable(variable_name, index)
    variable = get_local_store("@#{variable_name}")
    return variable[index].present?
  end

  # Per request cache
  def get_from_cache(name)
    instance_variable_get("@#{name}")
  end

  def set_in_cache(name, value)
    instance_variable_set("@#{name}", value)
  end
end
