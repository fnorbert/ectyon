# run: rake pull:instagrams
namespace :pull do
  desc "Pull all instagram data"
  task instagrams: :environment do
    Brand.instagram_credentials.each do |brand|
      brand.campaigns.each do |campaign|
        campaign.pull_instagrams
      end
    end
  end
end
