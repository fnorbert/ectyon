# run: rake remove:instagrams
namespace :remove do
  desc "Remove instagram data"
  task instagrams: :environment do
    instagrams = InstagramContent.order("id asc")
    last = instagrams.count - 1000
    if last > 0
      puts "Removed last #{last} isntagrams"
      instagrams.last(last).map(&:delete)
    else
      puts "No instagrams to destroy"
    end
  end

  desc "Remove twitter data"
  task tweets: :environment do
    tweets = TweetContent.order("id asc")
    last = tweets.count - 3000
    if last > 0
      puts "Removed last #{last} tweets"
      tweets.last(last).map(&:delete)
    else
      puts "No tweets to destroy"
    end
  end
end
