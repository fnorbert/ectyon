module ResqueLibraries
  class RemoveWeeklyInstagrams
    @queue = :general

    def self.perform(brand_id, campaign_id, instagrams)
      puts "Start remove_weekly_instagrams.rb"
      instagrams.group_by{|instagram| instagram["published_at"].to_time.beginning_of_week.strftime("%y-%m-%d")}
          .each do |week, instagrams|
        weekly_tweet_contents = WeeklyInstagramContent.find_or_create_by(week: week,
                                  brand_id: brand_id, campaign_id: campaign_id)
        weekly_instagram_content.total -= instagrams.count
        weekly_instagram_content.total_comments_count -= instagrams.map{|instagram| instagram["comments_count"]}.sum
        weekly_instagram_content.total_likes_count -= instagrams.map{|instagram| instagram["likes_count"]}.sum
        weekly_instagram_content.save
      end
      puts "End remove_weekly_instagrams.rb"
    end

  end
end
