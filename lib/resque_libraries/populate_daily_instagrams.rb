module ResqueLibraries
  class PopulateDailyInstagrams
    @queue = :general

    def self.perform(brand_id, campaign_id, instagrams)
      puts "Start populate_daily_instagrams.rb"
      instagrams.group_by{|instagram| instagram["published_at"].to_time.strftime("%y-%m-%d")}
          .each do |day, values|
        daily_instagram_content = DailyInstagramContent.find_or_create_by(day: day,
                                brand_id: brand_id, campaign_id: campaign_id)
        daily_instagram_content.total += values.count
        daily_instagram_content.total_comments_count += values.map{|instagram| instagram["comments_count"]}.sum
        daily_instagram_content.total_likes_count += values.map{|instagram| instagram["likes_count"]}.sum
        daily_instagram_content.save
      end
      puts "End populate_daily_instagrams.rb"
    end

  end
end
