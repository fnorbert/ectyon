module ResqueLibraries
  class PullTweets
    @queue = :general

    def self.perform(brand_id, campaign_id)
      puts "Start pull_tweets.rb"
      JTwitter::Search.new(brand_id, campaign_id).populate
      puts "End pull_tweets.rb"
    end

  end
end
