module ResqueLibraries
  class UpdateInstagrams
    @queue = :general

    def self.perform(brand_id, campaign_id, instagrams)
      puts "Start update_instagrams.rb"
      if instagrams.nil?
        JInstagram::Update.new(brand_id, campaign_id).existing
      else
        JInstagram::Update.new(brand_id, campaign_id).given(instagrams)
      end
      puts "End update_instagrams.rb"
    end

  end
end
