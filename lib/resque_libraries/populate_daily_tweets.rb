module ResqueLibraries
  class PopulateDailyTweets
    @queue = :general

    def self.perform(brand_id, campaign_id, tweets)
      puts "Start populate_daily_tweets.rb"
      tweets.group_by{|tweet| tweet["published_at"].to_time.strftime("%y-%m-%d")}
          .each do |day, values|
        daily_tweet_content = DailyTweetContent.find_or_create_by(day: day,
                                brand_id: brand_id, campaign_id: campaign_id)
        daily_tweet_content.total += values.count
        daily_tweet_content.total_favorite_count += values.map{|tweet| tweet["favorite_count"]}.sum
        daily_tweet_content.total_retweet_count += values.map{|tweet| tweet["retweet_count"]}.sum
        daily_tweet_content.total_reach += values.map{|tweet| tweet["reach"]}.sum
        daily_tweet_content.total_velocity += values.map{|tweet| tweet["velocity"]}.sum
        sentiment = Hash.new(0)
        values.each {|tweet| sentiment[tweet["sentiment"]] += 1}
        daily_tweet_content.total_positive += sentiment["positive"]
        daily_tweet_content.total_neutral += sentiment["neutral"]
        daily_tweet_content.total_negative += sentiment["negative"]
        daily_tweet_content.save
      end
      puts "End populate_daily_tweets.rb"
    end

  end
end
