module ResqueLibraries
  class PopulateWeeklyTweets
    @queue = :general

    def self.perform(brand_id, campaign_id, tweets)
      puts "Start populate_weekly_tweets.rb"
      tweets.group_by{|tweet| tweet["published_at"].to_time.beginning_of_week.strftime("%y-%m-%d")}
          .each do |week, values|
        weekly_tweet_content = WeeklyTweetContent.find_or_create_by(week: week,
                                  brand_id: brand_id, campaign_id: campaign_id)
        weekly_tweet_content.total += values.count
        weekly_tweet_content.total_favorite_count += values.map{|tweet| tweet["favorite_count"]}.sum
        weekly_tweet_content.total_retweet_count += values.map{|tweet| tweet["retweet_count"]}.sum
        weekly_tweet_content.total_reach += values.map{|tweet| tweet["reach"]}.sum
        weekly_tweet_content.total_velocity += values.map{|tweet| tweet["velocity"]}.sum
        sentiment = Hash.new(0)
        values.each {|tweet| sentiment[tweet["sentiment"]] += 1}
        weekly_tweet_content.total_positive += sentiment["positive"]
        weekly_tweet_content.total_neutral += sentiment["neutral"]
        weekly_tweet_content.total_negative += sentiment["negative"]
        weekly_tweet_content.save
      end
      puts "End populate_weekly_tweets.rb"
    end

  end
end
