module ResqueLibraries
  class PullInstagrams
    @queue = :general

    def self.perform(brand_id, campaign_id)
      puts "Start pull_instagrams.rb"
      JInstagram::Search.new(brand_id, campaign_id).populate
      puts "End pull_instagrams.rb"
    end

  end
end
