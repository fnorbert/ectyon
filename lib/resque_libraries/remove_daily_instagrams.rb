module ResqueLibraries
  class RemoveDailyInstagrams
    @queue = :general

    def self.perform(brand_id, campaign_id, instagrams)
      puts "Start remove_daily_instagrams.rb"
      instagrams.group_by{|instagram| instagram["published_at"].to_time.strftime("%y-%m-%d")}
          .each do |day, instagrams|
        daily_instagram_content = DailyInstagramContent.find_or_create_by(day: day,
                                brand_id: brand_id, campaign_id: campaign_id)

        daily_instagram_content.total -= instagrams.count
        daily_instagram_content.total_comments_count -= instagrams.map{|instagram| instagram["comments_count"]}.sum
        daily_instagram_content.total_likes_count -= instagrams.map{|instagram| instagram["likes_count"]}.sum
        daily_instagram_content.save
      end
      puts "End remove_daily_instagrams.rb"
    end

  end
end
