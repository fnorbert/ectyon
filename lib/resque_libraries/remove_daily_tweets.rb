module ResqueLibraries
  class RemoveDailyTweets
    @queue = :general

    def self.perform(brand_id, campaign_id, tweets)
      puts "Start remove_daily_tweets.rb"
      tweets.group_by{|tweet| tweet["published_at"].to_time.strftime("%y-%m-%d")}
          .each do |day, tweets|
        daily_tweet_content = DailyTweetContent.find_or_create_by(day: day,
                                brand_id: brand_id, campaign_id: campaign_id)
        daily_tweet_content.total -= tweets.count
        daily_tweet_content.total_favorite_count -= tweets.map{|tweet| tweet["favorite_count"]}.sum
        daily_tweet_content.total_retweet_count -= tweets.map{|tweet| tweet["retweet_count"]}.sum
        daily_tweet_content.total_reach -= tweets.map{|tweet| tweet["reach"]}.sum
        daily_tweet_content.total_velocity -= tweets.map{|tweet| tweet["velocity"]}.sum
        sentiment = Hash.new(0)
        tweets.each {|tweet| sentiment[tweet["sentiment"]] += 1}
        daily_tweet_content.total_positive -= sentiment["positive"]
        daily_tweet_content.total_neutral -= sentiment["neutral"]
        daily_tweet_content.total_negative -= sentiment["negative"]
        daily_tweet_content.save
      end
      puts "End remove_daily_tweets.rb"
    end

  end
end
