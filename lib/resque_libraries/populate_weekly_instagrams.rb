module ResqueLibraries
  class PopulateWeeklyInstagrams
    @queue = :general

    def self.perform(brand_id, campaign_id, instagrams)
      puts "Start populate_weekly_instagrams.rb"
      instagrams.group_by{|instagram| instagram["published_at"].to_time.beginning_of_week.strftime("%y-%m-%d")}
          .each do |week, values|
        weekly_instagram_content = WeeklyInstagramContent.find_or_create_by(week: week,
                                  brand_id: brand_id, campaign_id: campaign_id)
        weekly_instagram_content.total += values.count
        weekly_instagram_content.total_comments_count += values.map{|instagram| instagram["comments_count"]}.sum
        weekly_instagram_content.total_likes_count += values.map{|instagram| instagram["likes_count"]}.sum
        weekly_instagram_content.save
      end
      puts "End populate_weekly_instagrams.rb"
    end

  end
end
