module ResqueLibraries
  class RemoveWeeklyTweets
    @queue = :general

    def self.perform(brand_id, campaign_id, tweets)
      puts "Start remove_weekly_tweets.rb"
      tweets.group_by{|tweet| tweet["published_at"].to_time.beginning_of_week.strftime("%y-%m-%d")}
          .each do |week, tweets|
        weekly_tweet_contents = WeeklyTweetContent.find_or_create_by(week: week,
                                  brand_id: brand_id, campaign_id: campaign_id)
        weekly_tweet_contents.total -= tweets.count
        weekly_tweet_contents.total_favorite_count -= tweets.map{|tweet| tweet["favorite_count"]}.sum
        weekly_tweet_contents.total_retweet_count -= tweets.map{|tweet| tweet["retweet_count"]}.sum
        weekly_tweet_contents.total_reach -= tweets.map{|tweet| tweet["reach"]}.sum
        weekly_tweet_contents.total_velocity -= tweets.map{|tweet| tweet["velocity"]}.sum
        sentiment = Hash.new(0)
        tweets.each {|tweet| sentiment[tweet["sentiment"]] += 1}
        weekly_tweet_contents.total_positive -= sentiment["positive"]
        weekly_tweet_contents.total_neutral -= sentiment["neutral"]
        weekly_tweet_contents.total_negative -= sentiment["negative"]
        weekly_tweet_contents.save
      end
      puts "End remove_weekly_tweets.rb"
    end

  end
end
