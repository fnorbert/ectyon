module ResqueLibraries
  class UpdateTweets
    @queue = :general

    def self.perform(brand_id, campaign_id, tweets)
      puts "Start update_tweets.rb"
      if tweets.nil?
        JTwitter::Update.new(brand_id, campaign_id).existing
      else
        JTwitter::Update.new(brand_id, campaign_id).given(tweets)
      end
      puts "End update_tweets.rb"
    end

  end
end
