# bundle exec rails runner tstream.rb
loop do
  TwitterStream.find_each do |ts|
    brand = ts.brand
    if brand.has_twitter_credentials?
      if ts.status == "stop"
        JTwitter::Stream.new(ts.brand_id).stop
      elsif ts.status == "start"
        JTwitter::Stream.new(ts.brand_id).start
      end
    end
  end
  sleep 15
end
