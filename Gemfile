source 'https://rubygems.org'

ruby '2.4.1'

# Is an HTTP server for Rack applications
gem 'unicorn'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.0.3'
# Use pg as the database for Active Record
gem 'pg'
# MongoDB for stream results
gem 'mongoid'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
# Flexible authentication solution for Rails based on Warden
gem 'devise'
# HTML Abstraction Markup Language)
gem 'haml'
# Sass-powered version of Bootstrap 3
gem 'bootstrap-sass'
# framework for creating elegant backends for website administration
gem 'activeadmin', github: 'activeadmin'
gem 'active_material', github: 'vigetlabs/active_material'
gem 'rspec-rails', '>= 3.5.0.beta1'
gem 'ransack',    github: 'activerecord-hackery/ransack'
gem 'formtastic', github: 'justinfrench/formtastic'
gem 'draper',     github: 'audionerd/draper', branch: 'rails5', ref: 'e816e0e587'
# To fix a Draper deprecation error
gem 'activemodel-serializers-xml', github: 'rails/activemodel-serializers-xml'
# the Font-Awesome web fonts and stylesheets
gem "font-awesome-rails"
# a jQuery based replacement for select boxes
gem "select2-rails", "~> 3.5.3"
# the jQuery DataTables plugin
gem 'jquery-datatables-rails'
# Integrate Chart.js into Rails Asset Pipeline
gem 'chart-js-rails', "~> 0.0.9"
# the Twitter strategy for OmniAuth
gem 'omniauth-twitter'
# the Instagram strategy for OmniAuth
gem 'omniauth-instagram'
# the Facebook strategy for OmniAuth
gem 'omniauth-facebook'
# A Ruby interface to the Twitter API
gem 'twitter'
# The official gem for the Instagram API
gem 'instagram',  "~> 1.1.6"
# a library for bulk inserting data using ActiveRecord
gem 'activerecord-import'
# A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Rails 3 and 4
gem 'kaminari'
# Manage Procfile-based applications
gem 'foreman'
# Resque-scheduler is an extension to Resque that adds support for queueing items in the future.
gem 'resque-scheduler'
# Get information on the Resque queues
gem 'resque'
# Upload files in your Ruby applications, map them to a range of ORMs, store them on different backends
gem 'carrierwave'
# The Ruby cloud services library.
gem 'fog'
# A ruby wrapper for ImageMagick or GraphicsMagick command line
gem 'mini_magick'
# formtastic and simpleform
gem 'jasny_bootstrap_extension_rails'
# the Ruby library for Rollbar
gem 'rollbar'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # An IRB alternative and runtime developer console
  gem 'pry'
end

group :development do
  gem 'letter_opener'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background.
  gem 'spring'
end

group :production do
  # A performance management system
  gem 'newrelic_rpm'
  # Makes running your Rails app easier
  gem 'rails_12factor'
end
