Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  devise_scope :user do
    authenticated :user do
      root 'brands#index', as: :authenticated_root
    end
    unauthenticated do
      root 'pages#home', as: :unauthenticated_root
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#home'

  resources :users
  resources :brands do
    member do
      get :link_social_media
    end
    resources :campaigns do
      resources :tweets, only: [:index] do
        member do
          post :favorite
          post :unfavorite
          post :retweet
          post :unretweet
          post :follow
          post :unfollow
          get :reply
          post :post_reply
          get :comments
        end
        collection do
          post :order_by
          get :statistics, to: "twitter_statistics#index"
        end
      end
      resources :instagrams, only: [:index] do
        member do
          post :like
          post :unlike
          post :follow
          post :unfollow
          get :reply
          post :post_reply
          get :comments
        end
        collection do
          post :order_by
          get :statistics, to: "instagram_statistics#index"
        end
      end
      member do
        post :pull_tweets
        post :pull_instagrams
      end
    end
    resources :tweets, only: [:index] do
      member do
        post :favorite
        post :unfavorite
        post :retweet
        post :unretweet
        post :follow
        post :unfollow
        get :reply
        post :post_reply
        get :comments
      end
      collection do
        post :order_by
        get :statistics, to: "twitter_statistics#index"
      end
    end
    resources :instagrams, only: [:index] do
      member do
        post :like
        post :unlike
        post :follow
        post :unfollow
        get :reply
        post :post_reply
        get :comments
      end
      collection do
        post :order_by
        get :statistics, to: "instagram_statistics#index"
      end
    end
  end

  get '/auth/:provider/callback', to: 'sessions#create'
  post 'contact_us' => 'pages#contact_us', as: :contact_us
end
